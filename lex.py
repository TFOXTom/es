#
#   Epic Scripting
#   Version: 0.0.1 ALPHA
#   Main Developer: Tom Vos
#
#   File: lex.py
#   File Desc.: Contains the lexer function.
#

#List to store the tokens found
tokens = []

#Lexer function
def lex(content):

	#Some variables used in the lexer
	state = 0
	tok = ""
	string = ""
	content = list(content)

	#Loop through each character(char) in the file
	for char in content:

		#Add the current char to the current token
		tok += char

		#Check for valid tokens and reset if found
		if tok == " ":

			if state == 0:

				tok = ""

			else:

				tok = " "

		elif tok == "\n":

			tok = ""

		elif tok == "PRINTER":

			tokens.append("PRINTER")
			#print("FOUND A PRINTER")
			tok = ""

		elif tok ==  "\"":

			if state == 0:

				state = 1

			elif state == 1:

				tokens.append("STRING:" + string[1:])
				string = ""
				state = 0
				tok = ""
				#print("FOUND A STRING")

		elif state == 1:

			string += tok
			tok = ""

	#print(tokens) #Useful for debugging
	return tokens
