#
#   Epic Scripting
#   Version: 0.0.1 ALPHA
#   Main Developer: Tom Vos
#
#   File: parse.py
#   File Desc.: Contains the parse function.  
#

#Parse function
def parse(toks):

    #Index Variable
    i = 0

    #While loop to loop through all tokens(toks)
    while i < len(toks):

        #
        if toks[i] + " " + toks[i + 1][0:6] == "PRINTER STRING":
            print(toks[i+1][7:])
            i += 1
        i += 1
