#
#   Epic Scripting
#   Version: 0.0.1 ALPHA
#   Main Developer: Tom Vos
#
#   File: main.py
#   File Desc.: Main file.
#

#Imports
from sys import *
import lex
import parse

#Openfile function
def open_file(filename):

    #Read file, return content
    data = open(filename, "r").read()
    return data

#Run function(MAIN)
def run():

    #Check for file type/existence
    if len(argv) < 2:

        print ('No file specified. Make sure you add the "ES" file as an argument')
        return

    elif not argv[1].endswith(".tes"):

        print ('File specified does not have the ".tes" extension. Make sure it has!')
        return

    #Get file content(See: Openfile)
    data = open_file(argv[1])

    #Pass file content to lexer and receive tokens(See: lex.py)
    toks = lex.lex(data)

    #Pass tokens to parser(See: parse.py)
    parse.parse(toks)

run()
